import loginpage from "../support/pageobject/login.js" 
describe('POM', () => {
  it('login with valid credentials',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    
    ln.setEmail("useradmin1@gmail.com")
      ln.setPassword("password123")
      ln.clickSubmit()
      ln.verifylogin()
})
it('invalid email',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    
    ln.setEmail("useradmin@gmail.com")
      ln.setPassword("password123")
      ln.clickSubmit()
     ln.emptyfielderror()
     
})
it(' Empty password field',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    
    ln.setEmail("useradmin@gmail.com")
      
    ln.clickSubmit()
     ln.emptyfielderror()

})
it('Null fields',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    ln.clickSubmit()
     ln.emptyfielderror()

})
it('Null email field',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    
    ln.setPassword("password123")
      ln.clickSubmit()
     ln.emptyfielderror()
})
it ('unregistered email',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    ln.setEmail("testuser32@gmail.com")
    ln.setPassword("password123")
      ln.clickSubmit()
     ln.emptyfielderror()

})
it('wrong password',() => {
    cy.visit("https://next-realworld.vercel.app/user/login")
    const ln= new loginpage()
    ln.setEmail("useradmin@gmail.com")
    ln.setPassword("password12")
      ln.clickSubmit()
     ln.emptyfielderror()
})
})
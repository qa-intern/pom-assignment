describe('New Post Functionality Test', () => {
    beforeEach(() => {
        // Visit the website
        cy.visit('https://next-realworld.vercel.app/')
  
        // Click on the 'Sign in' button
        cy.contains('Sign in').click()
  
        // Fill in the login details
        cy.get('input[type="email"]').type('useradmin1@gmail.com')
        cy.get('input[type="password"]').type('password123')
        cy.contains('button','Sign in').click()
  
    
    })
  
    it('should handle submitting new post without body', () => {
        // Click on the 'New Post' button
        cy.contains('New Post').click()
  
        // Wait for the URL to change to the new post page
        cy.url().should('include', '/editor')
  
        // Fill in the post details without the body
        const testData = {
            title: 'Test Post Title',
            description: 'This is a test post description',
            tags: ['test', 'automation', 'cypress']
        }
  
        cy.get('input[placeholder="Article Title"]').type(testData.title)
        cy.get('input[placeholder="What\'s this article about?"]').type(testData.description)
  
        // Add tags
        testData.tags.forEach(tag => {
            cy.get('input[placeholder="Enter tags"]').type(tag)
            cy.get('input[placeholder="Enter tags"]').type('{enter}')
        })
  
        // Submit the new post
        cy.contains('button','Publish Article').click()
  
        
    })
  
    it('should handle submitting new post without tags', () => {
        // Click on the 'New Post' button
        cy.contains('New Post').click()
  
        // Wait for the URL to change to the new post page
        cy.url().should('include', '/editor')
  
        // Fill in the post details without tags
        const testData = {
            title: 'Test Post Title',
            description: 'This is a test post description',
            body: 'This is the body of the test post'
        }
  
        cy.get('input[placeholder="Article Title"]').type(testData.title)
        cy.get('input[placeholder="What\'s this article about?"]').type(testData.description)
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type(testData.body)
  
        // Submit the new post
        cy.contains('button','Publish Article').click()
  
        // Verify success message
        //cy.contains('Your article has been published!').should('be.visible')
    })
  
    it('should handle submitting new post without title', () => {
        // Click on the 'New Post' button
        cy.contains('New Post').click()
  
        // Wait for the URL to change to the new post page
        cy.url().should('include', '/editor')
  
        // Fill in the post details without the title
        const testData = {
            description: 'This is a test post description',
            body: 'This is the body of the test post',
            tags: ['test', 'automation', 'cypress']
        }
  
        cy.get('input[placeholder="What\'s this article about?"]').type(testData.description)
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type(testData.body)
  
        // Add tags
        testData.tags.forEach(tag => {
            cy.get('input[placeholder="Enter tags"]').type(tag)
            cy.get('input[placeholder="Enter tags"]').type('{enter}')
        })
  
        // Submit the new post
        cy.contains('button','Publish Article').click()
  
        // Verify error message for missing title
        //cy.contains('title can\'t be blank').should('be.visible')
    })
  
    it('should handle submitting new post without description', () => {
        // Click on the 'New Post' button
        cy.contains('New Post').click()
    
        // Wait for the URL to change to the new post page
        cy.url().should('include', '/editor')
    
        // Fill in the post details without the description
        const testData = {
            title: 'Assignment',
            body: 'This is the body of the test post',
            tags: ['test', 'automation', 'cypress']
        }
        
        cy.get('input[placeholder="Article Title"]').type(testData.title)
        cy.get('textarea[placeholder="Write your article (in markdown)"]').type(testData.body)
    
        // Add tags
        testData.tags.forEach(tag => {
            cy.get('input[placeholder="Enter tags"]').type(tag)
            cy.get('input[placeholder="Enter tags"]').type('{enter}')
        })
    
        // Submit the new post
        cy.contains('button','Publish Article').click()
  
        // Verify error message for missing description
        //cy.contains('description can\'t be blank').should('be.visible')
    })
  
    it('should handle submitting new post with empty fields', () => {
        // Click on the 'New Post' button
        cy.contains('New Post').click()
  
        // Wait for the URL to change to the new post page
        cy.url().should('include', '/editor')
  
        // Submit the new post without filling any fields
        cy.contains('button','Publish Article').click()
  
        // Verify error message for missing fields
        //cy.contains('article can\'t be blank').should('be.visible')
    })
  })
  import signup from "../support/pageobject/signup.js" 
  describe('POM', () => {
    it('signup with valid credentials',() => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      const ln=new signup(); 
      ln.setUsername("Adminnnnn")
      ln.setEmail("useradmin1@gmail.com")
      ln.setPassword("password123")
      ln.clickSubmit()
      ln.verifySignup()
})
it('signup with invalid email format',() => {
  cy.visit("https://next-realworld.vercel.app/user/register")
  const ln=new signup();
  ln.setUsername("Adminnnnn")
  ln.setEmail("useradmin1gmail.com")
  ln.setPassword("password123")
  ln.clickSubmit()
  ln.showErroremail()

})
it('Empty password field ',() => {
  cy.visit("https://next-realworld.vercel.app/user/register")
  const ln=new signup();
  ln.setUsername("Adminnnnn")
  ln.setEmail("useradmin1@gmail.com")
  //ln.setPassword("")
  ln.clickSubmit()
  ln.showErrorpassword()
  })
  it('Null fields ',() => {
    cy.visit("https://next-realworld.vercel.app/user/register")
    const ln=new signup();
   // ln.setUsername("Adminnnnn")
    //ln.setEmail("useradmin1@gmail.com")
    //ln.setPassword("")
    ln.clickSubmit()
    ln.showErroremail()
    ln.showErrorpassword()
  })
  it('Null username fields ',() => {
    cy.visit("https://next-realworld.vercel.app/user/register")
    const ln=new signup();
   // ln.setUsername("Adminnnnn")
    ln.setEmail("useradmin1@gmail.com")
    ln.setPassword("password123")
    ln.clickSubmit()
    ln.showErrorusername()
  })
  it('short password ',() => {
    cy.visit("https://next-realworld.vercel.app/user/register")
    const ln=new signup();
    ln.setUsername("Adminnnnn")
    ln.setEmail("useradmin1@gmail.com")
    ln.setPassword("Z")
    ln.clickSubmit()
    ln.verifySignup()
   
  })
  it('user number instead of username ',() => {
    cy.visit("https://next-realworld.vercel.app/user/register")
    const ln=new signup();
    ln.setUsername("123")
    ln.setEmail("useradmin023@gmail.com")
    ln.setPassword("Z")
    ln.clickSubmit()
    ln.verifySignup()
   
  })
  it('Duplicate email ',() => {
    cy.visit("https://next-realworld.vercel.app/user/register")
    const ln=new signup();
    ln.setUsername("userrrrrrrrsssssss")
    ln.setEmail("useradmin1@gmail.com")
    ln.setPassword("passwordssss")
    ln.clickSubmit()
    ln.showErroremail()
   
})
it('use registered username ',() => {
  cy.visit("https://next-realworld.vercel.app/user/register")
  const ln=new signup();
  ln.setUsername("Adminnnnn")
  ln.setEmail("useradmin26@gmail.com")
  ln.setPassword("passwordssss")
  ln.clickSubmit()
  ln.showErrorusername()
})
it.only('signup with special character username',() => {
  cy.visit("https://next-realworld.vercel.app/user/register")
  const ln=new signup();
  ln.setUsername("_test")
  ln.setEmail("test234@gmail.com")
  ln.setPassword("password123")
  ln.clickSubmit()
  ln.verifySignup()
  })
})
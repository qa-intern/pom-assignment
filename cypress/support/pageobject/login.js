class loginpage
{
   setEmail(Email)
  {
  
    cy.get(':nth-child(1) > .form-control').type(Email);
 
}
setPassword(password)
{

    cy.get(':nth-child(2) > .form-control').type(password);
  
}
clickSubmit()
{
  
    cy.get('.btn').click();
}
verifylogin()
{
  cy.get('.navbar-brand > span').should('have.text','conduit')
}
emptyfielderror(){
    cy.get('.error-messages').should('be.visible')
}

}
export default loginpage
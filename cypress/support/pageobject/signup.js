class signup 
{
  setUsername(username)
  {
    cy.get(':nth-child(1) > .form-control').type(username);
  }
  setEmail(Email)
  {
  
  cy.get(':nth-child(2) > .form-control').type(Email);
 
}
setPassword(password)
{

  cy.get(':nth-child(3) > .form-control').type(password);
  
}
clickSubmit()
{
  
  cy.get('.btn').click();
}
verifySignup()
{
  cy.get('.navbar-brand > span').should('have.text','conduit')
}
showErroremail()
{
    cy.get('#__next').should('be.visible')
   // cy.get(':nth-child(2) > .form-control').should('be.visible').and('contain', "please include an '@' in the email address")

}
showErrorpassword()
{
    cy.get('.error-messages').should ('be.visible')
}
showErrorusername()
{
    cy.get('.error-messages').should('be.visible')
}

}
export default signup